import 'dart:ui';

class Triangle {
  final int point;
  final Color color;
  Triangle(this.point, this.color);
}
